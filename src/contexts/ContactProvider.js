import React, { useState } from 'react';
import MesContacts from './ContactContext';

function MesContactsProvider(props){

    const [MesContacts, setMesContacts] = useState([]);

    return (

      <MesContacts.Provider
        value={{
            MesContacts,
            setAddContact:obj=>{
                setMesContacts([...MesContacts, obj])
            }
        }}
      >
        {props.children}
      </MesContacts.Provider>
    );
    }

    export default MesContactsProvider;